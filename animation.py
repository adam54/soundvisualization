import numpy as np

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.path as path
import matplotlib.animation as animation

from configuration import *


class Animation:
    def __init__(self, frequency_streamer):
        self._frequency_streamer = frequency_streamer

    def start(self):
        bins = np.linspace(MIN_FREQUENCY, MAX_FREQUENCY, FREQUENCY_CHUNK + 1)
        n = np.ones(FREQUENCY_CHUNK)

        left = np.array(bins[:-1])
        right = np.array(bins[1:])
        bottom = np.zeros(len(left))
        top = bottom + n
        nrects = len(left)

        nverts = nrects * (1 + 3 + 1)
        verts = np.zeros((nverts, 2))
        codes = np.ones(nverts, int) * path.Path.LINETO
        codes[0::5] = path.Path.MOVETO
        codes[4::5] = path.Path.CLOSEPOLY
        verts[0::5, 0] = left
        verts[0::5, 1] = bottom
        verts[1::5, 0] = left
        verts[1::5, 1] = top
        verts[2::5, 0] = right
        verts[2::5, 1] = top
        verts[3::5, 0] = right
        verts[3::5, 1] = bottom

        patch = None

        def animate(i):
            if self._frequency_streamer.has_next():
                n = self._frequency_streamer.next()
                top = bottom + n
                verts[1::5, 1] = top
                verts[2::5, 1] = top
            return [patch, ]

        fig, ax = plt.subplots()
        barpath = path.Path(verts, codes)
        patch = patches.PathPatch(
            barpath, facecolor='green', edgecolor='yellow', alpha=0.5)
        ax.add_patch(patch)

        ax.set_xlim(MIN_FREQUENCY, MAX_FREQUENCY)
        ax.set_ylim(MIN_VOLUME, MAX_VOLUME)

        ax.set_title('Sound visualization')

        ax.set_xlabel('frequency [Hz]')
        ax.set_ylabel('volume [dB]')

        ani = animation.FuncAnimation(fig, animate, repeat=False, blit=True,
                                      interval=ANIMATION_INTERVAL)
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        plt.show()
