from microphone import MicrophoneReader
from animation import Animation
from frequency import FrequencyStreamer
from converter import FFTConverter

if __name__ == '__main__':
    microphone_reader = MicrophoneReader()
    fft_converter = FFTConverter()
    frequency_streamer = FrequencyStreamer(microphone_reader, fft_converter)
    histogram_animation = Animation(frequency_streamer)

    histogram_animation.start()
