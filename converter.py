import numpy as np
import math
from configuration import *


class FFTConverter:
    def __init__(self):
        self.volume_abs = MAX_VOLUME - MIN_VOLUME

    def convert(self, data):
        freq = np.abs(np.fft.fft(data))
        freq = self.scale(freq)
        freq = self.cut(freq)
        return freq

    def scale(self, freq):
        nmin = np.min(freq)
        nmax = np.max(freq)
        nabs = nmax - nmin

        f = lambda x: (x - nmin) / nabs * self.volume_abs
        vf = np.vectorize(f)
        return vf(freq)

    def cut(self, freq):
        return freq[1:FREQUENCY_CHUNK + 1]
