from configuration import *
from threading import Thread
from pyaudio import PyAudio
import numpy as np
import time


class MicrophoneReader:
    def __init__(self):
        self._updated = False
        self._readingThread = Thread(target=self._read_from_stream)
        self._stopped = True
        self._data = np.array([])

    def get_data(self):
        self._updated = False
        return self._data

    def is_updated(self):
        return self._updated

    def is_started(self):
        return not self._stopped

    def start(self):
        self._stopped = False
        self._readingThread.start()

    def _read_from_stream(self):
        audio = PyAudio()

        stream = audio.open(
            format=audio.get_format_from_width(WIDTH),
            channels=CHANNELS,
            rate=RATE,
            input=True,
            output=True,
            frames_per_buffer=CHUNK)

        while not self._stopped:
            try:
                self._data = np.fromstring(stream.read(CHUNK, exception_on_overflow=False), dtype=np.int16)\
                    .astype(np.float64)
                self._updated = True
            except IOError as e:
                print(str(e))
                pass

        if stream:
            stream.stop_stream()
            stream.close()

        if audio:
            audio.terminate()

    def stop(self):
        self._stopped = True
        self._readingThread.join()
