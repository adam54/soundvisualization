import numpy as np


class FrequencyStreamer:
    def __init__(self, microphone_reader, fft_converter):
        self._microphone_reader = microphone_reader
        self.fft_converter = fft_converter
        self.next_data = np.array([])

    def has_next(self):
        if not self._microphone_reader.is_started():
            self._microphone_reader.start()
        if self._microphone_reader.is_updated():
            mic_input = np.array(self._microphone_reader.get_data())
            self.next_data = self.fft_converter.convert(mic_input)
            return True
        else:
            return False

    def next(self):
        result = self.next_data
        self.next_data = None
        return result
